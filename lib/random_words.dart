import 'package:flutter/material.dart';
import 'package:super_hero/super_hero.dart';

class RandomWords extends StatefulWidget {
  @override
  RandomWordsState createState() => RandomWordsState();
}

class RandomWordsState extends State<RandomWords> {
  var randomWords = <String>[]; //[] defines array/list
  var savedWords = <String>[];

  Widget buildList() {
    return ListView.builder(
        padding: const EdgeInsets.all(16.0),
        itemBuilder: (context, item) {
          if (item.isOdd) return Divider();

          final index = item ~/ 2;

          if (index >= randomWords.length) {
            randomWords.add(SuperHero.random());
          }

          return buildRow(randomWords[index]);
        });
  }

  Widget buildRow(String randomWord) {
    final alreadySaved = savedWords.contains(randomWord);

    return ListTile(
      title: Text(randomWord, style: TextStyle(fontSize: 20.0)),
      trailing: Icon(alreadySaved ? Icons.favorite : Icons.favorite_border,
          color: alreadySaved ? Colors.purple : null),
      onTap: () {
        setState(() {
          if (alreadySaved) {
            savedWords.remove(randomWord);
          } else {
            savedWords.add(randomWord);
          }
        });
      },
    ); //ListTile = each row
  }

  void pushSaved() {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (BuildContext context) {
      final Iterable<ListTile> tiles = savedWords.map((String word) {
        return ListTile(title: Text(word, style: TextStyle(fontSize: 18.0)));
      });

      final List<Widget> divided =
          ListTile.divideTiles(context: context, tiles: tiles).toList();

      return Scaffold(
          appBar: AppBar(title: Text('Saved Super Heroes')),
          body: ListView(
            children: divided,
          ));
    }));
  }

  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Super Hero Generator'),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.list),
              onPressed: pushSaved,
            )
          ],
        ),
        body: buildList());
  }
}
